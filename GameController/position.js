class Position {
  constructor(column, row) {
    this.column = column;
    this.row = row;
  }

  isEquals(position) {
    return this.column === position.column && this.row === position.row;
  }
}

module.exports = Position;
