const assert = require("assert").strict;
const letters = require("../GameController/letters.js");
const position = require("../GameController/position.js");
const { getNextLetter, unwrap } = require("./unwrap");

describe("unwrap", function () {
  it("should icrement", function () {
    assert.deepStrictEqual(getNextLetter(letters.B), letters.C);
  });

  it("should return positions for right", function () {
    var start = new position(letters.B, 3);
    assert.deepStrictEqual(unwrap(start, 2, "right"), [
      start,
      new position(letters.C, 3),
    ]);
  });

  it("should return positions for bottom", function () {
    var start = new position(letters.B, 7);

    assert.deepStrictEqual(unwrap(start, 2, "bottom"), [
      start,
      new position(letters.B, 8),
    ]);
  });

  it("should return null for wrong bottom", function () {
    var start = new position(letters.B, 3);

    assert.deepStrictEqual(unwrap(start, 7, "bottom"), null);
  });

  it("should return null for wrong right", function () {
    var start = new position(letters.C, 3);

    assert.deepStrictEqual(unwrap(start, 7, "right"), null);
  });
});
