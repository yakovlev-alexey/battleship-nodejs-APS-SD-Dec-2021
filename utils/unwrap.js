const letters = require("../GameController/letters.js");
const position = require("../GameController/position.js");

function getNextLetter(letter) {
  return letters.get(letters.get(letter).value + 1);
}

function unwrap(pos, size, direction) {
  var result = [pos];

  if (direction === "right") {
    let currLetter = pos.column;

    if (letters.get(currLetter).value + size > 9) {
      return null;
    }

    while (size > 1) {
      currLetter = getNextLetter(currLetter);
      result.push(new position(currLetter, pos.row));
      size--;
    }
  } else if (direction === "bottom") {
    let currRow = pos.row;

    if (currRow + size > 9) {
      return null;
    }

    while (size > 1) {
      currRow += 1;
      result.push(new position(pos.column, currRow));
      size--;
    }
  }

  return result;
}

module.exports = {
  getNextLetter,
  unwrap,
};
